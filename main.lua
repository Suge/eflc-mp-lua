require("/filterscripts/utilis")
require("/filterscripts/scriptLoader")

--I load the following scripts like this so I can reload them whenever using the rcon script
loadScript("rcon")
loadScript("playerSpawn")
loadScript("carSpawning")
loadScript("weapons")
loadScript("skins")
--loadScript("customChat")

local currentWeather = 0

function weatherChanger()
	print("Weather change")
	currentWeather = currentWeather + 1
	if currentWeather > 7 then 
		--Resets the World weather 0 and hours to midnight
		currentWeather = 0
		setWorldTime(1, 0, 0)
	end
	
	setWorldWeather(1, currentWeather)
end

setTimer("weatherChanger", 10000, 0)
