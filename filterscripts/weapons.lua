local weaponsDialogId = 100

local weapons = {
	{1, "Baseball Bat"},
	{2, "Pool Cue"},
	{3, "Knife"},
	{4, "Grenade"},
	{5, "Molotov"},
	{7, "Pistol"},
	{9, "Desert Eagle"},
	{10, "Shotgun"},
	{11, "Beretta"},
	{12, "Micro SMG"},
	{13, "SMG"},
	{14, "Assault Rifle"},
	{15, "Carbine Rifle"},
	{16, "Sniper"},
	{17, "Sniper2"},
	{18, "RPG"},
	{29, "Revolver"},
	{31, "Auto shotgun"},
	{32, "Assault SMG"},
	{34, "MG"},
	{35, "Advanced Sniper"},
	{41, "Parachute"}
}
	
local function createDialog()
	local diagLoaded = createDialogList(weaponsDialogId, "Weapons List", 1, "Get!", "Cancel")
	if(diagLoaded ~= true) then
		print("The weapons dialog wasn't created")
		return
	end
	
	for id, weapData in pairs(weapons) do
		addDialogRow(weaponsDialogId, weapData[2])
	end
end
createDialog()

function onWeaponsCommand(playerid, text)
	if(string.sub(text, 1, 8) == "/weapons") then
		showDialogList(playerid, weaponsDialogId)
	end
end
registerEvent("onWeaponsCommand", "onPlayerCommand")

function onWeaponsDialogResponse(playerid, dialogId, buttonId, dialogRow)
	if(dialogId == weaponsDialogId and buttonId == 1 and weapons[dialogRow + 1] ~= nil) then
		givePlayerWeapon(playerid, weapons[dialogRow + 1][1], 600)
	end
end
registerEvent("onWeaponsDialogResponse", "onPlayerDialogResponse")

function weaponsUnload()
	print("Unloading weapons")
	deleteDialogList(weaponsDialogId)
	unregisterEvent("onWeaponsChat")
	unregisterEvent("onWeaponsDialogResponse")
end
registerScriptUnload("weapons", weaponsUnload) --Adds an unload point