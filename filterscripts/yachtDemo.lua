function playerChatEvent(playerid, text)
    if(string.sub(text, 1, 5) == "/yacht") then

        local x, y, z = getPlayerPos(playerid)
        local boatObjs = {1239854102, 2219185248, 67157199, 1930762713, 1233545547, 1005833766}

        for i, obj in ipairs(boatObjs) do
            createObject(x, y, z, 0.0, 0.0, 0.0, 0.0, obj, 1, false, 0, 255)
        end     
    end
end
registerEvent("playerChatEvent", "onPlayerChat")