local spawnedCars = {}

function carSpawningUnload()
	print("Unloading carSpawning")
	unregisterEvent("carSpawningChatEvent")
	unregisterEvent("carSpawningPlayerLeft")
	deleteTimer("speedometer")
	
	for spawnerId, carId in pairs(spawnedCars) do
		if(isVehicle(carId)) then
			deleteVehicle(carId)
		end
	end
end
registerScriptUnload("carSpawning", carSpawningUnload) --Adds an unload point

local function deletePlayerVehicles(playerid)
	if(spawnedCars[playerid] ~= nil) then
		local carId = spawnedCars[playerid]
		if(isVehicle(carId)) then
			deleteVehicle(carId)
		end
	end 
end

function speedometer(lstring, linteger, ldouble, lbool)
	local players = getPlayers()
	
	local vehicleId = 0
	local velocityX, velocityY, velocityZ = 0.0, 0.0, 0.0
	local speed = 0
	
	for i, id in ipairs(players) do
		vehicleId = isPlayerInAnyVehicle(id)
		if(vehicleId ~= 0 and isVehicle(vehicleId)) then
			velocityX, velocityY, velocityZ = getVehicleVelocity(vehicleId)
			speed = math.ceil(math.sqrt(velocityX * velocityX + velocityY * velocityY + velocityY * velocityY) * 1.609);
			drawInfoText(id, "~b~" .. speed .. " mph", 600)
		end
	end
end
setTimer("speedometer", 500, 0)

function carSpawningCommandEvent(playerid, text)
	if(string.sub(text, 1, 2) == "/v") then
		local modelName = string.upper(string.sub(text, 4))
		local modelId = getVehicleModelId(modelName)
		if(modelId == -1) then
			sendPlayerMsg(playerid, "'" .. modelName .. "' is an invalid vehicle name", 0xFFFF0000)
			return
		end
		local x, y, z = getPlayerPos(playerid)
		local rColor = math.random(0, 170)
		deletePlayerVehicles(playerid)
		local carId = createVehicle(modelId, x, y, z, 0.0, 0.0, 0.0, rColor, rColor, rColor, rColor, getPlayerWorld(playerid))
		spawnedCars[playerid] = carId
		sendPlayerMsg(playerid, modelName .. " spawned with id " .. carId .. " with color " .. rColor, 0xFFFFFFFF)			
	end
end
registerEvent("carSpawningCommandEvent", "onPlayerCommand")

function carSpawningPlayerLeft(playerid, reason)
	if(spawnedCars[playerid] ~= nil and isVehicle(spawnedCars[playerid])) then
		deleteVehicle(spawnedCars[playerid])
		spawnedCars[playerid] = nil
	end
end
registerEvent("carSpawningPlayerLeft", "onPlayerLeft")